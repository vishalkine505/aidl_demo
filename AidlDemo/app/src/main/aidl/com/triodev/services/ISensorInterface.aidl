// ISensorInterface.aidl
package com.triodev.services;

// Declare any non-default types here with import statements

interface ISensorInterface {
    String orientation();
}
