package com.triodev.aidldemo

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import com.triodev.services.ISensorInterface
import android.widget.TextView


class MainActivity : AppCompatActivity() {

    private lateinit var orientationText: TextView
    private var orientationInterface: ISensorInterface? = null
    private var orientationServiceConnection: ServiceConnection? = null
    private var orientationServiceIntent: Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        orientationText = findViewById(R.id.sensor_data)
        addSensorDataObserver()
        bindOrientationService()
    }

    private fun bindOrientationService() {
        val implicitIntent = Intent()
        implicitIntent.action = "com.triodev.sensor.service.AIDL"
        orientationServiceIntent = convertImplicitIntentToExplicitIntent(implicitIntent, applicationContext)
        orientationServiceConnection = object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName?, binder: IBinder?) {
                orientationInterface = ISensorInterface.Stub.asInterface(binder)
                getDataFromAIDL()
            }

            override fun onServiceDisconnected(componentName: ComponentName?) {
            }
        }
        orientationServiceIntent?.let {
            orientationServiceConnection?.let {
                bindService(
                    orientationServiceIntent,
                    it,
                    Context.BIND_AUTO_CREATE
                )
            }
        }
    }

    private fun getDataFromAIDL() {
        orientationInterface?.orientation()?.let {
            orientationText.text = it
        }
    }

    private fun addSensorDataObserver() {
        Handler(Looper.getMainLooper()).postDelayed({
            getDataFromAIDL()
        }, 2)

    }

    private fun convertImplicitIntentToExplicitIntent(
        implicitIntent: Intent?,
        context: Context
    ): Intent? {
        val pm: PackageManager = context.packageManager
        val resolveInfoList =
            pm.queryIntentServices(implicitIntent!!, 0)
        if (resolveInfoList == null || resolveInfoList.size != 1) {
            return null
        }
        val serviceInfo = resolveInfoList[0]
        val component =
            ComponentName(serviceInfo.serviceInfo.packageName, serviceInfo.serviceInfo.name)
        val explicitIntent = Intent(implicitIntent)
        explicitIntent.component = component
        return explicitIntent
    }
}