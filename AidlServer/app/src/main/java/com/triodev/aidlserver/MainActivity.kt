package com.triodev.aidlserver

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<View>(R.id.button)
        button.setOnClickListener {
            val implicitIntent = Intent()
            implicitIntent.action = "com.triodev.sensor.service.AIDL"
            val context = applicationContext
            val explicitIntent =
                convertImplicitIntentToExplicitIntent(implicitIntent, context)
            if (explicitIntent != null) {
                if (context.startService(explicitIntent) != null)
                    Toast.makeText(
                        this,
                        "Service is already running",
                        Toast.LENGTH_SHORT
                    ).show();
            } else {
                Toast.makeText(
                    this,
                    "There is no service running, starting service..",
                    Toast.LENGTH_SHORT
                ).show();
            }
        }
    }


private fun convertImplicitIntentToExplicitIntent(
    implicitIntent: Intent?,
    context: Context
): Intent? {
    val pm: PackageManager = context.packageManager
    val resolveInfoList =
        pm.queryIntentServices(implicitIntent!!, 0)
    if (resolveInfoList == null || resolveInfoList.size != 1) {
        return null
    }
    val serviceInfo = resolveInfoList[0]
    val component =
        ComponentName(serviceInfo.serviceInfo.packageName, serviceInfo.serviceInfo.name)
    val explicitIntent = Intent(implicitIntent)
    explicitIntent.component = component
    return explicitIntent
}
}